<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConditionResource;
use App\Models\Condition;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ConditionController extends Controller
{
    const VALIDATION_RULES = [
        'text' => 'required|string'
    ];

    public function getConditions(Request $request)
    {
        $Conditions = Condition::with('conditions')->get();
        return response()->json([
            'data' => Conditionresource::collection($Conditions)
        ], Response::HTTP_OK);
    }

    public function getCondition(Request $request, int $id)
    {
        $Condition = Condition::with('conditions')->find($id);
        if (is_null($Condition)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return response()->json([
            'data' => ConditionResource::collection(collect([$Condition]))->first()
        ], Response::HTTP_OK);
    }

    public function createCondition(Request $request)
    {
        $validator = Validator::make($request->all(), self::VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        Condition::create($validator->validated());
        return response()->json([], Response::HTTP_CREATED);
    }

    public function updateCondition(Request $request, int $id)
    {
        $condition = Condition::find($id);
        if (is_null($condition)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $condition->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deleteCondition(Request $request, int $id)
    {
        $condition = Condition::find($id);
        if (is_null($condition)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $condition->delete();
        return response()->json([], Response::HTTP_OK);
    }
}
