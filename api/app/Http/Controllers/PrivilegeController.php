<?php

namespace App\Http\Controllers;

use App\Http\Resources\PrivilegeResource;
use App\Models\Privilege;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PrivilegeController extends Controller
{
    const VALIDATION_RULES = [
      'text' => 'required|string'
    ];

    public function getPrivileges(Request $request)
    {
        $privileges = Privilege::with('conditions')->get();
        return response()->json([
            'data' => Privilegeresource::collection($privileges)
            ], Response::HTTP_OK);
    }

    public function getPrivilege(Request $request, int $id)
    {
        $privilege = Privilege::with('conditions')->find($id);
        if (is_null($privilege)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return response()->json([
            'data' => PrivilegeResource::collection(collect([$privilege]))->first()
        ], Response::HTTP_OK);
    }

    public function createPrivilege(Request $request)
    {
        $validator = Validator::make($request->all(), self::VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        Privilege::create($validator->validated());
        return response()->json([], Response::HTTP_CREATED);
    }

    public function updatePrivilege(Request $request, int $id)
    {
        $privilege = Privilege::find($id);
        if (is_null($privilege)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $privilege->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deletePrivilege(Request $request, int $id)
    {
        $privilege = Privilege::find($id);
        if (is_null($privilege)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $privilege->delete();
        return response()->json([], Response::HTTP_OK);
    }
}
