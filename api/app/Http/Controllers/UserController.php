<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    const CREATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|max:255|min:8',
    ];

    const UPDATE_VALIDATION_RULES = [
        'name' => 'nullable|string|max:255',
        'description' => 'nullable'
    ];

    public function getUsers(Request $request)
    {
        $users = User::with(['posts', 'comments'])->get();
        return response()->json([
            'data' => UserResource::collection($users)
        ], Response::HTTP_OK);
    }

    public function getUser(Request $request, int $id)
    {
        $user = User::with(['privileges'])->find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return response()->json([
            'data' => UserResource::collection(collect([$user]))->first()
        ], Response::HTTP_OK);
    }

    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), self::CREATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        User::create($validator->validated());
        return response()->json([], Response::HTTP_CREATED);
    }

    public function updateUser(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::UPDATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $user->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deleteUser(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $user->delete();
        return response()->json([], Response::HTTP_OK);
    }

}

