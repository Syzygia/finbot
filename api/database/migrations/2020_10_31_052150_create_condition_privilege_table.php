<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConditionPrivilegeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condition_privilege', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('condition_id');
            $table->unsignedBigInteger('privilege_id');
            $table->foreign('condition_id')->on('conditions')->references('id')->cascadeOnDelete();
            $table->foreign('privilege_id')->on('privileges')->references('id')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condition_privilege');
    }
}
