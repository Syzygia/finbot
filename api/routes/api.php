<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([ 'namespace' => 'App\Http\Controllers\Api' ], function () {
    Route::group([ 'prefix' => 'users' ], function () {
        Route::get('', 'UserController@getUsers');
        Route::get('{id}', 'UserController@getUser');
        Route::post('', 'UserController@createUser');
        Route::post('{id}', 'UserController@updateUser');
        Route::delete('{id}', 'UserController@deleteUser');
    });
    Route::group([ 'prefix' => 'privileges' ], function () {
        Route::get('', 'PrivilegeController@getPrivileges');
        Route::get('{id}', 'PrivilegeController@getPrivilege');
        Route::post('', 'PrivilegeController@createPrivilege');
        Route::post('{id}', 'PrivilegeController@updatePrivilege');
        Route::delete('{id}', 'PrivilegeController@deletePrivilege');
    });
    Route::group([ 'prefix' => 'conditions' ], function () {
        Route::get('', 'ConditionController@getConditions');
        Route::get('{id}', 'ConditionController@getCondition');
        Route::post('', 'ConditionController@createCondition');
        Route::post('{id}', 'ConditionController@updateCondition');
        Route::delete('{id}', 'ConditionController@deleteCondition');
    });
});
